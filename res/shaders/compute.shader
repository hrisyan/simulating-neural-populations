#version 460 core
layout(local_size_x = 1, local_size_y = 1, local_size_z = 1) in;
layout(rgba32f, binding = 0) uniform image2D screen;
layout(std430, binding = 3) buffer layoutName
{	
	highp float M[200][700];
	float positions[200][700][8];
	int positionsSize[200];
	float areaN[200][700];
	int binN[1024][1024][2];
	int spikeTrains[200][700];
	int resetSize[200][700];
	float reset[200][700][5][3];
	int dSetSize[200][700];
	float dSet[200][700][7][3];

}ssboData;

uniform float time;


void isLeftLine(float x, float y, float x1, float y1, float x2, float y2, out int ans) {

	if (((x - x1) * (y2 - y1) - (y - y1) * (x2 - x1)) <= 0) {

		ans = 1;

	}
	else {
		ans = 0;
	}

}
void isPointInQuad(float x, float y, float x1, float y1, float x2, float y2, float x3, float y3, float x4, float y4, out float ans) {
	int a, b, c, d;

	isLeftLine(x, y, x1, y1, x2, y2, a);
	isLeftLine(x, y, x2, y2, x3, y3, b);
	isLeftLine(x, y, x3, y3, x4, y4, c);
	isLeftLine(x, y, x4, y4, x1, y1, d);
	if (a == 1 && b == 1 && c == 1 && d == 1) ans = 1;
	else ans = 0;

}

void convertX(float x, out float xo) {
	xo = (x + 0.066) * (1 / 0.0195);
}
void convertY(float y, out float yo) {
	yo = (y /1.5);
}

//vertecies entered in couter clockwise order
void calculateArea(float x1, float y1, float x2, float y2, float x3, float y3, float x4, float y4, out float area) {


	float a = (x1 * y2 - y1 * x2) + (x2 * y3 - y2 * x3) + (x3 * y4 - y3 * x4) + (x4 * y1 - y4 * x1);
	area = a / 2;

}

void binFinder(float x, float y, out int xBin, out int yBin, out float area) {
	int i,j;
	int t = 0;
	xBin = -1;
	yBin = -1;
	area = 0;
	for (i = 0; i < 200; i++) {
		int size = ssboData.positionsSize[i];
	
		for (j = 0; j < size; j++) {
			float x1, y1, x2, y2, x3, y3, x4, y4, a;

			convertX(ssboData.positions[i][j][0], x1);
			convertY(ssboData.positions[i][j][1], y1);
			convertX(ssboData.positions[i][j][2], x2);
			convertY(ssboData.positions[i][j][3], y2);
			convertX(ssboData.positions[i][j][4], x3);
			convertY(ssboData.positions[i][j][5], y3);
			convertX(ssboData.positions[i][j][6], x4);
			convertY(ssboData.positions[i][j][7], y4);

			isPointInQuad(x, y, x1, y1, x2, y2, x3, y3, x4, y4, a);

			if (a == 1) {
				xBin = i;
				yBin = j;
				float area_t;
				calculateArea(x1, y1, x2, y2, x3, y3, x4, y4, area_t);
				area = area_t;
				break;
				
			}
		}
		if (area > 0) {
			break;
		}
	}

}
//Generating pseudo-random value
float randv2(vec2 c) {
	return fract(sin(dot(c.xy, vec2(12.9898, 78.233))) * 43758.5453);
}

float factorial(float k) {
	int n = int(k);
	float f = 1.0;
	for (int i = 1; i <= n; ++i) {
		f *= float(i);
	}
	return f;
}
//The Probability of k spikes in a Poisson Distribution
float prob(float k, float t, float m) {

	float r = 1.25;
	float _lambda = t * m * r;
	float a = pow(2.71828, (-_lambda));

	float p = ((pow(2.71828, (-_lambda))) * pow(_lambda, k)) / factorial(k);
	return p;
}

void main()
{
	

	ivec2 pixel_coords = ivec2(gl_WorkGroupID.xy);

	ivec2 dims = imageSize(screen);

	float x = (float(pixel_coords.x) / dims.x);
	float y = (float(pixel_coords.y) / dims.y);

	vec4 pixel;
	float _area,area;
	int _index,binX,binY;
	int xBin, yBin;

	highp int time_int = int(time);

	
	if (time_int == 0) {

		pixel = vec4(0, 1, 0, 1.0);
		binFinder(x, y, xBin,yBin, _area);
		ssboData.binN[pixel_coords.x][pixel_coords.y][0] = xBin;
		ssboData.binN[pixel_coords.x][pixel_coords.y][1] = yBin;
		ssboData.areaN[xBin][yBin] = _area;
	
	}

	else {

		pixel = vec4(1, 1, 1, 1.0);
		int mt = 0;


		
		binX = ssboData.binN[pixel_coords.x][pixel_coords.y][0];
		binY = ssboData.binN[pixel_coords.x][pixel_coords.y][1];
		area = ssboData.areaN[binX][binY];
		
		int z = 0;
		//Spike for [0][0]
		if (time_int % 2 == 1)
		{
			ssboData.spikeTrains[binX][binY] = -1;
			ssboData.spikeTrains[0][0] = -1;
			
		}else if(time_int % 2 == 0) {
			if (binX == 10 && binY == 10&& ssboData.spikeTrains[0][0]==-1) {
				
				float r = randv2(vec2(x, y) * float(time_int));
				float p = prob(0.0, 32, ssboData.M[0][0]);
				if (r > p) {
					//Spike
					int binSize = int(ssboData.dSetSize[0][0]);
					highp float sum = 0;
					z = 1;
					for (int i = 0; i < binSize; i++) {

						highp int toX = int(ssboData.dSet[0][0][i][0]);
						highp int toY = int(ssboData.dSet[0][0][i][1]);
						highp float frac = ssboData.dSet[0][0][i][2];
						highp float move = ssboData.M[0][0] * frac;
						int toY_ = (ssboData.positionsSize[toX] + toY - time_int) % ssboData.positionsSize[toX];

						ssboData.M[toX][toY_] += move;
						ssboData.M[0][0] -= move;


					}
					


				}
				ssboData.spikeTrains[0][0] = z;

			}
		}
		if (binX != -1 && binY != -1) {
			int t = time_int;
			int mt = (binY - t) % int(ssboData.positionsSize[binX]);
			//mt = binY;

			float density;

			////Handling the threshold
			if (ssboData.resetSize[binX][binY] > 0) {


				int binSize = int(ssboData.resetSize[binX][binY]);

				for (int i = 0; i < binSize; i++) {

					highp int toX = int(ssboData.reset[binX][binY][i][0]);
					highp int toY = int(ssboData.reset[binX][binY][i][1]);
					highp float frac = ssboData.reset[binX][binY][i][2];
					highp float move = ssboData.M[binX][mt] * frac;
					int toY_ = (ssboData.positionsSize[toX] + toY - time_int)  % ssboData.positionsSize[toX];

					ssboData.M[toX][toY_] += move;


				}
				ssboData.M[binX][mt] = 0;

			}
			//
			if (time_int % 2 == 0 && ssboData.spikeTrains[binX][binY] == -1) {
				float r = randv2(vec2(x, y) * float(time_int));
				float p = prob(0.0, 32, ssboData.M[binX][mt]);
				if (r > p) {
					//Spike
					int binSize = int(ssboData.dSetSize[binX][binY]);
					highp float sum = 0;
					for (int i = 0; i < binSize; i++) {

						highp int toX = int(ssboData.dSet[binX][binY][i][0]);
						highp int toY = int(ssboData.dSet[binX][binY][i][1]);
						highp float frac = ssboData.dSet[binX][binY][i][2];
						highp float move = ssboData.M[binX][mt] * frac;
						int toY_ = (ssboData.positionsSize[toX] + toY - time_int) % ssboData.positionsSize[toX];

						ssboData.M[toX][toY_] += move;
						ssboData.M[binX][mt] -= move;


					}
					


				}
			}
			//Handling the mass leaving the strip 
			if (binY == 0 && ssboData.M[binX][mt]>0 && binX > 0 && binX < 199) {
				ssboData.M[0][0] += ssboData.M[binX][mt];
				ssboData.M[binX][mt] = 0;				
			}

			density = ssboData.M[binX][mt] / area;
	
			if (density > 0.1 && density < 1) {
				pixel = vec4(density / 1.5, 0, 0, 1.0);
			}
			else if (density / 50 > 0.1 && density / 50 < 1) {
				pixel = vec4(density / 25, density / 50, 0, 1.0);
			}
			else if (density / 100 > 0.1 && density / 100 < 1) {
				pixel = vec4(density / 100, density / 100, 0, 1.0);
			}
			else {
				pixel = vec4((density / 400), (density / 400), (density / 400), 1.0);
			}		

		}
	}
		
			
	

	imageStore(screen, pixel_coords, pixel);
}
