# Simulating Neural Populations
The application was developed in C++ and OpenGL version 4.6 was used alongside with glad as loading library and GLFW for window management in addition the GLSL shading language was also used. Furthermore, from the GL_EXTENSIONS the ARB_arrays_of_arrays is a prerequisite
## Usage
Visual Studio 2022 was used for the development of the project. The solution file is included in the code submission, which links all of the libraries, alternatively, the code can be compiled using g++ by including the libraries' paths in the command.
## Result

<img src="https://gitlab.com/hrisyan/simulating-neural-populations/-/raw/main/ezgif-1-34e9acdf54.gif" width="400" height="400" />
