#include "Texture.h"
#include "Debug.h"
#include"glad/glad.h"


Texture::Texture() {
	
	GLCall(glCreateTextures(GL_TEXTURE_2D, 1, &screenTex));
	GLCall(glTextureParameteri(screenTex, GL_TEXTURE_MIN_FILTER, GL_NEAREST));
	GLCall(glTextureParameteri(screenTex, GL_TEXTURE_MAG_FILTER, GL_NEAREST));
	GLCall(glTextureParameteri(screenTex, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE));
	GLCall(glTextureParameteri(screenTex, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE));
	GLCall(glTextureStorage2D(screenTex, 1, GL_RGBA32F, SCREEN_WIDTH, SCREEN_HEIGHT));
	GLCall(glBindImageTexture(0, screenTex, 0, GL_FALSE, 0, GL_WRITE_ONLY, GL_RGBA32F));
}
Texture::~Texture() {
	glBindTextureUnit(0, 0);
}

void Texture::Bind() const
{
	GLCall(glBindTextureUnit(0, screenTex));	
}

void Texture::Unbind() const
{
	GLCall(glBindTextureUnit(0, 0));
}
