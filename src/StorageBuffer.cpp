#include"glad/glad.h"
#include "StorageBuffer.h"
#include "Debug.h"

StorageBuffer::StorageBuffer(const void* data, unsigned int size) {
	
	GLCall(glGenBuffers(1, &m_RendererID));
	GLCall(glBindBuffer(GL_SHADER_STORAGE_BUFFER, m_RendererID));
	GLCall(glBufferData(GL_SHADER_STORAGE_BUFFER, size, data, GL_DYNAMIC_COPY));
}
StorageBuffer::~StorageBuffer()
{
	GLCall(glDeleteBuffers(1, &m_RendererID));
}

void StorageBuffer::Bind(unsigned int binding) const
{
	GLCall(glBindBufferBase(GL_SHADER_STORAGE_BUFFER, binding, m_RendererID));
}

void StorageBuffer::Unbind() const
{
	GLCall(glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0));
}
