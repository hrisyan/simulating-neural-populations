//#include "Renderer.h"
#include "Shader.h"
#include "Debug.h"

#include <iostream>
#include <string>
#include <fstream>
#include <sstream>



Shader::Shader(const std::string& filepathV, const std::string& filepathF, const std::string& filepathC)
{
    ShaderProgramSource source = ReadShaders(filepathV, filepathF, filepathC);

    CreateShader(source.VertexSource, source.FragmentSource, source.ComputeSource);

}
Shader::~Shader()
{
    GLCall(glDeleteProgram(programs.computeShaderProgram));
    GLCall(glDeleteProgram(programs.screenShaderProgram));
}

void Shader::Bind(int type) const
{
    if (type) {
        GLCall(glUseProgram(programs.computeShaderProgram));
    }
    else {
        GLCall(glUseProgram(programs.screenShaderProgram));
    }
    
}

void Shader::Unbind() const
{
    GLCall(glUseProgram(0));
}

int Shader::GetUniformLocation(const std::string& name)
{
    if (m_UniformLocationCache.find(name) != m_UniformLocationCache.end())
        return m_UniformLocationCache[name];
    int location=-1;
    int location_c = GLCall(glGetUniformLocation(programs.computeShaderProgram, name.c_str()));
    int location_vf = GLCall(glGetUniformLocation(programs.screenShaderProgram, name.c_str()));
    if (location_c != -1) {
        location = location_c;
    }else if (location_vf != -1) {
        location = location_vf;
    }
    else {
        std::cout << "No active uniform variable with name " << name << " found" << std::endl;
    }

    m_UniformLocationCache[name] = location;

    return location;
}
void Shader::SetUniform1f(const std::string& name, float value)
{
    GLCall(glUniform1f(GetUniformLocation(name), value));
}

void Shader::SetUniform1i(const std::string& name, float value)
{
    GLCall(glUniform1i(GetUniformLocation(name), value));
}

void Shader::ReadShader(const std::string& filepath, int type) {
    std::ifstream stream(filepath);
    std::string line;
    
    while (getline(stream, line))
    {
        ss[type] << line << '\n';
    }
    

}
struct ShaderProgramSource Shader::ReadShaders(const std::string& filepathV, const std::string& filepathF, const std::string& filepathC) {

    ReadShader(filepathV, 0);
    ReadShader(filepathF, 1);
    ReadShader(filepathC, 2);
    struct ShaderProgramSource sps = { ss[0].str(), ss[1].str(), ss[2].str()};
    return sps;
}
unsigned int Shader::CompileShader(unsigned int type, const std::string& source)
{
    unsigned int id = GLCall(glCreateShader(type));
    const char* src = source.c_str();
    GLCall(glShaderSource(id, 1, &src, nullptr));
    GLCall(glCompileShader(id));

    // Error handling
    int result;
    GLCall(glGetShaderiv(id, GL_COMPILE_STATUS, &result));
    //std::cout << "Shader:\n " << src;
    std::cout << (type == GL_VERTEX_SHADER ? "vertex" : type == GL_FRAGMENT_SHADER ? "fragment" : "compute") << " shader compile status: " << result << std::endl;
    if (result == GL_FALSE)
    {
        int length;
        GLCall(glGetShaderiv(id, GL_INFO_LOG_LENGTH, &length));
        char* message = (char*)alloca(length * sizeof(char));
        GLCall(glGetShaderInfoLog(id, length, &length, message));
        std::cout
            << "Failed to compile "
            << (type == GL_VERTEX_SHADER ? "vertex" : type == GL_FRAGMENT_SHADER ? "fragment" : "compute")
            << "shader"
            << std::endl;
        std::cout << message << std::endl;
        
        GLCall(glDeleteShader(id));
        return 0;
    }

    return id;
}



void Shader::CreateShader(const std::string& vertexShader, const std::string& fragmentShader, const std::string& computeShader)
{

    GLuint screenShaderProgram = GLCall(glCreateProgram());
    unsigned int vs = CompileShader(GL_VERTEX_SHADER, vertexShader);
    unsigned int fs = CompileShader(GL_FRAGMENT_SHADER, fragmentShader);
    GLCall(glAttachShader(screenShaderProgram, vs));
    GLCall(glAttachShader(screenShaderProgram, fs));
    GLCall(glLinkProgram(screenShaderProgram));

    GLint program_linked;

    GLCall(glGetProgramiv(screenShaderProgram, GL_LINK_STATUS, &program_linked));
    std::cout << "Screen program link status: " << program_linked << std::endl;
    if (program_linked != GL_TRUE)
    {
        GLsizei log_length = 0;
        GLchar message[1024];
        GLCall(glGetProgramInfoLog(screenShaderProgram, 1024, &log_length, message));
        std::cout << "Failed to link screen program" << std::endl;
        std::cout << message << std::endl;
    }

    GLCall(glValidateProgram(screenShaderProgram));
    GLCall(glDeleteShader(vs));
    GLCall(glDeleteShader(fs));

    unsigned int cs = CompileShader(GL_COMPUTE_SHADER, computeShader);    

    GLuint computeProgram = GLCall(glCreateProgram());
    GLCall(glAttachShader(computeProgram, cs));
    GLCall(glLinkProgram(computeProgram));

    GLCall(glGetProgramiv(screenShaderProgram, GL_LINK_STATUS, &program_linked));
    std::cout << "Compute program link status: " << program_linked << std::endl;
    if (program_linked != GL_TRUE)
    {
        GLsizei log_length = 0;
        GLchar message[1024];
        GLCall(glGetProgramInfoLog(screenShaderProgram, 1024, &log_length, message));
        std::cout << "Failed to link screen program" << std::endl;
        std::cout << message << std::endl;
    }

    GLCall(glValidateProgram(screenShaderProgram));

    programs.computeShaderProgram = computeProgram;
    programs.screenShaderProgram = screenShaderProgram;

}
