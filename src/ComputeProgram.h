#pragma once

#include "Debug.h"
#include "StorageBuffer.h"
#include "Shader.h"


class ComputeProgram
{
public:
    void ShowWorkgroupMaxSize();
    void Compute(const StorageBuffer& ssbo, const Shader& shader) const;
};
