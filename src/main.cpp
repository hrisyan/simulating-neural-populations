#include<iostream>
#include <vector>

#include "glad/glad.h"
#include "GLFW/glfw3.h"
#include "MeshReader.h"
#include "Shader.h"
#include "Texture.h"
#include "VertexArray.h"
#include "VertexBuffer.h"
#include "IndexBuffer.h"
#include "Renderer.h"
#include "StorageBuffer.h"
#include "ComputeProgram.h"



const unsigned short OPENGL_MAJOR_VERSION = 4;
const unsigned short OPENGL_MINOR_VERSION = 6;


GLfloat vertices[] =
{
	-1.0f, -1.0f , 0.0f, 0.0f, 0.0f,
	-1.0f,  1.0f , 0.0f, 0.0f, 1.0f,
	 1.0f,  1.0f , 0.0f, 1.0f, 1.0f,
	 1.0f, -1.0f , 0.0f, 1.0f, 0.0f,
};


GLuint indices[] =
{
	0, 2, 1,
	0, 3, 2
};
/*
The M 200 by 700 matrix holds the probability mass of all bins and is
initialised with the generateMass() function. The positions matrix
is 200 by 700 by 8 and holds all the vertices for all the bins read
from the file. The choice of using a variable for size positionsSize
stems from the fact that half of the strips have size 500 the other 700
and the memory is used to improve performance. Likewise the reset
and resetSize which is the reset set and the dSet and dSetSize
is the displacement set. The areaN and binN matrices are values
saved after the first cycle - the area of each bin and for each pixel
the bin it is in.
*/
struct mainData {
	GLfloat M[200][700];
	GLfloat positions[200][700][8];
	GLint positionsSize[200];		
	GLfloat areaN[200][700];	
	GLint binN[1024][1024][2];
	GLint spikeTrains[200][700];
	GLint resetSize[200][700];
	GLfloat reset[200][700][5][3];
	GLint dSetSize[200][700];
	GLfloat dSet[200][700][7][3];
}main_data;

void convertReset(std::vector<std::vector<std::vector<std::vector<float>>> >& vals) {
	for (int i = 0; i < vals.size(); i++)
	{		
		for (unsigned int j = 0; j < vals[i].size(); j++)
		{
			for (unsigned int z = 0; z < vals[i][j].size(); z++)
			{
				main_data.resetSize[i][j] = vals[i][j].size();
				
				for (unsigned int h = 0; h < vals[i][j][z].size(); h++)
				{
					main_data.reset[i][j][z][h] = vals[i][j][z][h];
					
				}
			}
		}
	}
}

void convertDSet(std::vector<std::vector<std::vector<std::vector<float>>> >& vals) {
	for (int i = 0; i < vals.size(); i++)
	{
		for (unsigned int j = 0; j < vals[i].size(); j++)
		{
			for (unsigned int z = 0; z < vals[i][j].size(); z++)
			{
				main_data.dSetSize[i][j] = vals[i][j].size();				

				for (unsigned int h = 0; h < vals[i][j][z].size(); h++)
				{
					main_data.dSet[i][j][z][h] = vals[i][j][z][h];				


				}
				

			}
		}

	}
	
}
void convert3dV(std::vector<std::vector<std::vector<float>> >& vals)
{
	for ( int ih = 0; ih < vals.size(); ih++)
	{		
		main_data.positionsSize[ih] = vals[ih].size();
		int size = vals[ih].size();
		
		for (unsigned int j = 0; j < size; j++)
		{
			
			for (unsigned int z = 0; z < vals[ih][j].size(); z++)
			{				
				
				main_data.positions[ih][j][z] = vals[ih][j][z];
		
				
			}
			
		
		}		
		

	}
	
}
//Generating Mass where i is the max strip and j is the max bin
void generateMass() {
	
	float sum = 0.0;
	int i = 0;
	int j = 0;
	while (sum != 1.0) {

		int dif = (1 - sum) * 100000;
		GLfloat a;

		a = std::rand() % 30 + 3;

		sum += (a / 100000);
		if (i == 200) {
			sum += (1 - sum);
		}
		main_data.M[i][j] = (a / 100000.0f);

		if (j == 700) {
			j = 0;
			i++;
		}
		else {
			j++;
		}


	}	


}


int main()
{
	

	glfwInit();

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, OPENGL_MAJOR_VERSION);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, OPENGL_MINOR_VERSION);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);

	GLFWwindow* window = glfwCreateWindow(SCREEN_WIDTH, SCREEN_HEIGHT, "Simulating Neural Populations", NULL, NULL);
	if (!window)
	{
		std::cout << "Failed to create the GLFW window\n";
		glfwTerminate();
	}
	glfwMakeContextCurrent(window);
	//performing buffer swaps as soon as possible when rendering for a frame is finished
	//glfwSwapInterval(1); 
	//Having a minimum wait time of 1 frame
	glfwSwapInterval(1);
	
	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
	{
		std::cout << "Failed to initialize OpenGL context" << std::endl;
	}
	glViewport(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);

	//the VertexArray is initialised with the values
	//of two triangles that span across the whole screen 
	//and the points that mark the coroners of texture
	VertexArray va;
	VertexBuffer vb(vertices, 4 * 5* sizeof(float));
	IndexBuffer ib(indices, 6);
	VertexBufferLayout layout;
	layout.AddFloat(3);
	va.AddBuffer(vb, layout);
	layout.AddFloat(2);
	va.AddBuffer(vb, layout);

	//Reading the shader files
	Shader shader("res/shaders/vertex.shader", "res/shaders/fragment.shader", "res/shaders/compute.shader");
	
	Texture texture;	
	Renderer renderer;
	renderer.Clear();
	ComputeProgram comp;
	MeshReader m("res/condC.model");
	m.DSReader("res/cond_0_0.05_0.mat");
	
	//Converting the vectors into matrices
	convertReset(m.reset);
	convertDSet(m.dSet);	
	convert3dV(m.positions);
	generateMass();
	
	//all of the data is arranged and
	//then given to the SSBO which is bound to the compute shader.
	StorageBuffer ssbo(&main_data, sizeof(main_data));

	//Rendering loop
	while (!glfwWindowShouldClose(window))
	{
		//The Compute call takes as parameters:
		//	1. A compute shader read from compute.
		//shader file in the Shader class.
		//	2. A Shader Storage Buffer Object which consist
		//of all the data need for the computation.
		comp.Compute(ssbo, shader);
		//we declare a static time variable which is send to the shader and then incremented.
		static int t = 0;
		shader.SetUniform1f("time", t);
		t++;		
		//Texture send to compute and vertex shaders
		shader.SetUniform1i("screen", 0);
		//Executing the Draw call with the vertex array, index buffer, vertex and fragment
		//shaders and the changed texture.
		renderer.Draw(va, ib, shader,texture);		
		
		glfwSwapBuffers(window);
		glfwPollEvents();
	}


	glfwDestroyWindow(window);
	glfwTerminate();
}
