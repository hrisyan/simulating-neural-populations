#include "MeshReader.h"

#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <vector>

std::vector<float> split(const std::string& s, char delim) {
	std::vector<float> elems;
	std::stringstream ss;
	ss.str(s);
	std::string item;
	while (std::getline(ss, item, delim)) {
		//std::cout << item << "\n";
		elems.push_back(std::stof(item));
	}
	return elems;
}
std::vector<int> splitI(const std::string& s, char delim) {
	std::vector<int> elems;
	std::stringstream ss;
	ss.str(s);
	std::string item;
	while (std::getline(ss, item, delim)) {
		//std::cout << item << "\n";
		elems.push_back(std::stoi(item));
	}
	return elems;
}
std::vector<std::string> splitStr(const std::string& s, char delim) {
	std::vector<std::string> elems;
	std::stringstream ss;
	ss.str(s);
	std::string item;
	while (std::getline(ss, item, delim)) {
		//std::cout << item << "\n";
		elems.push_back(item);
	}
	return elems;
}
void MeshReader::init() {
	reset.resize(200);
	dSet.resize(200);
	for (int i = 0; i < 200; i++) {
		
		reset[i].resize(700);	
		dSet[i].resize(700);
	}
	
}
MeshReader::MeshReader(const std::string& filepath) {
	init();
	std::ifstream stream(filepath);
	std::string line;	
	int x=0;

	std::string v[4];
	std::vector<std::vector<float>> stB;
	std::vector<float> s{ -0.06505, 0, -0.06505, 0.005, -0.06495, 0.005, -0.06495, 0 };
	stB.push_back(s);
	positions.push_back(stB);
	int rs = 0;
	while (getline(stream, line))
	{


		if (line.find("<Strip>") != std::string::npos) {
			x++;
			if (x == 1) {
				continue;
			}
			
				//std::cout << line << "\n";
				/*v[x] = line;*/
				std::string str = line.substr(9);
				
				str = str.substr(0, str.size() - 10);


				std::vector<float> strip = split(str, ' ');
				int j = 0;
				std::vector<float> s;
				std::vector<std::vector<float>> Ss;
				
				for (int i = 1; i <= strip.size(); i++) {


					s.push_back(strip[i - 1]);
					if (i % 8 == 0) {
						Ss.push_back(s);
						//positions[x].push_back(s);
						s.clear();
					}
				}

				positions.push_back(Ss);
			
			
		}

		if (line.find("Reset") != std::string::npos) {

			
			
			while (getline(stream, line))
			{
				try
				{
					std::vector<std::string> strip = splitStr(line, '\t');					
					std::vector<int> from = splitI(strip[0], ',');
					std::vector<int> to = splitI(strip[1], ',');
					//std::cout << from[0] << " " << from[1] << "\n";
					//std::cout << to[0] << " " << to[1] << "\n";
					std::vector<float> row;
					row.push_back(to[0]);
					row.push_back(to[1]);
					row.push_back(stof(strip[2]));					
					reset[from[0]][from[1]].push_back(row);
				}
				catch (const std::exception&)
				{

				}
				
				
			}
			
		}

		
	}


}
void MeshReader::DSReader(const std::string& filepath) {
	std::ifstream stream(filepath);
	std::string line;
	getline(stream, line);
	while (getline(stream, line))
	{
		std::vector<std::string> strip = splitStr(line, ';');
		std::vector<int> index= splitI(strip[1], ',');
		for (int i = 2; i<strip.size(); i++) {
			std::vector<std::string> to = splitStr(strip[i], ':');
			std::vector<int> toI = splitI(to[0], ',');
			std::vector<float> row;
			row.push_back(toI[0]);
			row.push_back(toI[1]);
			row.push_back(stof(to[1]));
			//std::cout << index[0] << " " << index[1] << " " << toI[0] << " " << toI[1] << " " << stof(to[1]) << "\n";
			dSet[index[0]][index[1]].push_back(row);
		}
		

	}
	
}