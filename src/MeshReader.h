#pragma once
#define MAX_N 1000


#include <string>
#include <iostream>
#include <vector>

class MeshReader
{

public:
    std::vector<std::vector<std::vector<float>>> positions;

    std::vector<std::vector<std::vector<std::vector<float>>>> reset;
    std::vector<std::vector<std::vector<std::vector<float>>>> dSet;
    
    MeshReader(const std::string& filepath);
   void DSReader(const std::string& filepath);
    void init();

   
};
