#pragma once

class StorageBuffer
{
private:
    unsigned int m_RendererID;

public:
    StorageBuffer(const void* data, unsigned int size);
    ~StorageBuffer();

    void Bind(unsigned int binding) const;
    void Unbind() const;
};

