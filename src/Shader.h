#pragma once

#include <string>
#include <unordered_map>
#include <sstream> 

struct ShaderProgramSource
{
    std::string VertexSource;
    std::string FragmentSource;
    std::string ComputeSource;
};

struct ShaderProgram
{
    unsigned int screenShaderProgram;
    unsigned int computeShaderProgram;
};

class Shader
{
private:
    ShaderProgram programs;   
    std::stringstream ss[3];    
    std::unordered_map<std::string, int> m_UniformLocationCache;


public:
    Shader(const std::string& filepathV, const std::string& filepathF, const std::string& filepathC);
    ~Shader();

    void Bind(int type) const;
    void Unbind() const;

    void SetUniform1f(const std::string& name, float value);
    void SetUniform1i(const std::string& name, float value);
    void SetUniform1fv(const std::string& name, float value);

private:
    int GetUniformLocation(const std::string& name);
    void ReadShader(const std::string& filepath, int type);
    struct ShaderProgramSource ReadShaders(const std::string& filepathV, const std::string& filepathF, const std::string& filepathC);
    unsigned int CompileShader(unsigned int type, const std::string& source);
    void CreateShader(const std::string& vertexShader, const std::string& fragmentShader, const std::string& computeShader);
   

};
