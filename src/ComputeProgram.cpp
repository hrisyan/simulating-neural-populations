#include "ComputeProgram.h"
#include <iostream>
#define X_WORKGROUP 1024
#define Y_WORKGROUP 1024
#define Z_WORKGROUP 1

void ComputeProgram::ShowWorkgroupMaxSize() {

	int work_grp_cnt[3];
	GLCall(glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_COUNT, 0, &work_grp_cnt[0]));
	GLCall(glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_COUNT, 1, &work_grp_cnt[1]));
	GLCall(glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_COUNT, 2, &work_grp_cnt[2]));
	std::cout << "Max work groups per compute shader" <<
		" x:" << work_grp_cnt[0] <<
		" y:" << work_grp_cnt[1] <<
		" z:" << work_grp_cnt[2] << "\n";

	int work_grp_size[3];
	GLCall(glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_SIZE, 0, &work_grp_size[0]));
	GLCall(glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_SIZE, 1, &work_grp_size[1]));
	GLCall(glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_SIZE, 2, &work_grp_size[2]));
	std::cout << "Max work group sizes" <<
		" x:" << work_grp_size[0] <<
		" y:" << work_grp_size[1] <<
		" z:" << work_grp_size[2] << "\n";

	int work_grp_inv;
	GLCall(glGetIntegerv(GL_MAX_COMPUTE_WORK_GROUP_INVOCATIONS, &work_grp_inv));
	std::cout << "Max invocations count per work group: " << work_grp_inv << "\n";
}
void ComputeProgram::Compute(const StorageBuffer& ssbo, const Shader& shader) const {
	shader.Bind(1);
	ssbo.Bind(3);
	GLCall(glDispatchCompute(X_WORKGROUP, Y_WORKGROUP, Z_WORKGROUP));
	GLCall(glMemoryBarrier(GL_ALL_BARRIER_BITS));
}