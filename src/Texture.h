#pragma once

const unsigned int SCREEN_WIDTH = 1024;
const unsigned int SCREEN_HEIGHT = 1024;

class Texture {
private:
    unsigned int screenTex;
   

public:
    Texture();
    ~Texture() ;

    void Bind() const;
    void Unbind()const;
};
